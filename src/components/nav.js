import React from 'react';
import { inject, observer } from 'mobx-react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';

const CustNavBar = inject('appStore', 'stateStore')(
  observer(props => {
    const { navIsOpen } = props.stateStore.navIsOpen;
    const { toggleNavOpen } = props.appStore;
    return (
      <Navbar expand="lg" color="light" light>
        <NavbarBrand href="/">Beer Demo</NavbarBrand>
        <NavbarToggler onClick={event => toggleNavOpen(event)} />
        <Collapse isOpen={navIsOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink exact activeClassName="active" className="nav-link" to="/">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink activeClassName="active" className="nav-link" to="/beers">
                Beers
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink activeClassName="active" className="nav-link" to="/admin">
                Admin
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  })
);

export default CustNavBar;
