import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import BeerListContainer from '../containers/beerListContainer';
import NavBar from './nav';

const BeersPage = props => (
  <Container fluid>
    <NavBar />
    <Row>
      <Col>
        <BeerListContainer />
      </Col>
    </Row>
  </Container>
);

export default BeersPage;
