import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import NavBar from './nav';

const title = 'Beer Demo - React';

const Home = props => (
  <Container fluid>
    <NavBar />
    <Row>
      <Col>
        <h1>{title}</h1>
      </Col>
    </Row>
  </Container>
);

export default Home;
