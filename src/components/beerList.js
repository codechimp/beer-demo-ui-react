import React from 'react';
import { inject, observer } from 'mobx-react';

import { Table } from 'reactstrap';
import Paginator from './paginator';

const Beerlist = inject('appStore', 'stateStore')(
  observer(props => {
    const { beers, beersPage, handlePageChange } = props.stateStore;
    return (
      <div>
        <Table striped hover size="sm">
          <thead>
            <tr>
              <td>Name</td>
              <td>Style</td>
              <td>Brewery</td>
              <td>Description</td>
              <td>ABV</td>
              <td>IBU</td>
              <td>Rating</td>
            </tr>
          </thead>
          <tbody>
            {beers.map((beer, idx) => (
              <tr key={beer.id}>
                <td>{beer.name}</td>
                <td>{beer.style}</td>
                <td>{beer.brewery}</td>
                <td>{beer.description}</td>
                <td>{beer.abv}</td>
                <td>{beer.ibu}</td>
                <td />
              </tr>
            ))}
          </tbody>
        </Table>
        {beersPage === undefined || beersPage === null ? null : (
          <Paginator
            activePage={beersPage.number}
            itemsCountPerPage={beersPage.size}
            totalItemsCount={beersPage.totalElements}
            pageRangeDisplayed={10}
            onChange={props.handlePageChange}
          />
        )}
      </div>
    );
  })
);

export default Beerlist;
