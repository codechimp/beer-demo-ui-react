import React from 'react';
import _ from 'lodash';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

class Paginator extends React.Component {
  constructor(props) {
    super(props);
    const { activePage, itemsCountPerPage, totalItemsCount, pageRangeDisplayed, onChange } = props;
    const displayedPage = activePage + 1;
    const lowestPage = displayedPage == 1 ? 1 : displayedPage - pageRangeDisplayed / 2;
    const lastPage = Math.ceil(totalItemsCount / itemsCountPerPage);
    this.state = {
      itemsCountPerPage: itemsCountPerPage,
      totalItemsCount: totalItemsCount,
      displayRangeMin: lowestPage,
      displayRangeMax: Math.min(lowestPage + pageRangeDisplayed - 1, lastPage),
      displayedPage: displayedPage,
      displayedRange: pageRangeDisplayed,
      lastPage: lastPage,
    };
  }

  static getDerivedStateFromProps(props, state) {
    console.log('props: ', props);
    const { activePage, itemsCountPerPage, totalItemsCount, pageRangeDisplayed, onChange } = props;
    const newDisplayedPage = activePage + 1;
    const lastPage = Math.ceil(totalItemsCount / itemsCountPerPage);

    if (
      newDisplayedPage != state.displayedPage ||
      itemsCountPerPage != state.itemsCountPerPage ||
      pageRangeDisplayed != state.displayedRange
    ) {
      console.log('Returning new derived state');
      const lowestPage =
        newDisplayedPage <= state.displayRangeMin || newDisplayedPage >= state.displayRangeMax
          ? Math.max(newDisplayedPage - pageRangeDisplayed / 2, 1)
          : state.displayRangeMin;
      console.log('lowestPage: ', lowestPage);
      console.log('pageRangeDisplayed: ', pageRangeDisplayed);
      console.log('lowestPage + pageRangeDisplayed - 1: ', lowestPage + pageRangeDisplayed - 1);
      console.log('lastPage: ', lastPage);
      const highestPage = Math.min(lowestPage + pageRangeDisplayed - 1, lastPage);
      console.log('highestPage: ', highestPage);
      return {
        itemsCountPerPage: itemsCountPerPage,
        totalItemsCount: totalItemsCount,
        displayRangeMin: lowestPage,
        displayRangeMax: highestPage,
        displayedPage: newDisplayedPage,
        displayedRange: pageRangeDisplayed,
        lastPage: lastPage,
      };
    } else {
      console.log('No derived state changes');
      return null;
    }
  }

  render() {
    console.log('props: ', this.props);
    const { onChange } = this.props;
    const { displayedPage, displayRangeMin, displayRangeMax, lastPage } = this.state;
    return (
      <Pagination size="sm">
        <PaginationItem id={`page_item_prev`} disabled={displayedPage <= 1}>
          <PaginationLink previous onClick={() => onChange(displayedPage > 1 ? displayedPage : 1)} />
        </PaginationItem>
        {_.range(displayRangeMin, displayRangeMax + 1).map(page => {
          return (
            <PaginationItem key={page} active={page == displayedPage}>
              <PaginationLink id={`page_${page}`} onClick={() => onChange(page)}>
                {page}
              </PaginationLink>
            </PaginationItem>
          );
        })}
        <PaginationItem id={`page_item_next`} disabled={displayedPage >= lastPage}>
          <PaginationLink next onClick={() => onChange(displayedPage + 1)} />
        </PaginationItem>
      </Pagination>
    );
  }
}

export default Paginator;
