import React from 'react';
import Paginator from './paginator';
import renderer from 'react-test-renderer';
import { PaginationItem } from 'reactstrap';

test('Paginator initializes with 10 pages correctly', () => {
  var clickedPage = null;
  const component = renderer.create(
    <Paginator
      activePage={0}
      itemsCountPerPage={20}
      totalItemsCount={500}
      pageRangeDisplayed={10}
      onChange={page => (clickedPage = page)}
    />
  );
  const root = component.root;
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  expect(root.find(el => el.props.id == 'page_item_prev').props.disabled).toBe(true);
  expect(root.find(el => el.props.id == 'page_item_next').props.disabled).toBe(false);
  let paginationLinks = root.findAll(el => el.props.className == 'page-link');
  expect(paginationLinks.length).toBe(12);
  expect(paginationLinks[1].children).toEqual(['1']);
  expect(paginationLinks[2].children).toEqual(['2']);
  expect(paginationLinks[3].children).toEqual(['3']);
  expect(paginationLinks[4].children).toEqual(['4']);
  expect(paginationLinks[5].children).toEqual(['5']);
  expect(paginationLinks[6].children).toEqual(['6']);
  expect(paginationLinks[7].children).toEqual(['7']);
  expect(paginationLinks[8].children).toEqual(['8']);
  expect(paginationLinks[9].children).toEqual(['9']);
  expect(paginationLinks[10].children).toEqual(['10']);

  // Click on the 10th item and make sure calls the onClick appropriately
  root.find(el => el.props.id == 'page_10').props.onClick(10);
  expect(clickedPage).toEqual(10);

  // Moving to the 10th page should display correct range of pages
  component.update(
    <Paginator
      activePage={9}
      itemsCountPerPage={20}
      totalItemsCount={500}
      pageRangeDisplayed={10}
      onChange={page => (clickedPage = page)}
    />
  );
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  expect(root.find(el => el.props.id == 'page_item_prev').props.disabled).toBe(false);
  expect(root.find(el => el.props.id == 'page_item_next').props.disabled).toBe(false);
  paginationLinks = root.findAll(el => el.props.className == 'page-link');
  expect(paginationLinks.length).toBe(12);
  expect(paginationLinks[1].children).toEqual(['5']);
  expect(paginationLinks[2].children).toEqual(['6']);
  expect(paginationLinks[3].children).toEqual(['7']);
  expect(paginationLinks[4].children).toEqual(['8']);
  expect(paginationLinks[5].children).toEqual(['9']);
  expect(paginationLinks[6].children).toEqual(['10']);
  expect(paginationLinks[7].children).toEqual(['11']);
  expect(paginationLinks[8].children).toEqual(['12']);
  expect(paginationLinks[9].children).toEqual(['13']);
  expect(paginationLinks[10].children).toEqual(['14']);

  // Moving to the last page (page 24) should diplay the correct range of pages
  component.update(
    <Paginator
      activePage={24}
      itemsCountPerPage={20}
      totalItemsCount={500}
      pageRangeDisplayed={10}
      onChange={page => (clickedPage = page)}
    />
  );
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  expect(root.find(el => el.props.id == 'page_item_prev').props.disabled).toBe(false);
  expect(root.find(el => el.props.id == 'page_item_next').props.disabled).toBe(true);
  paginationLinks = root.findAll(el => el.props.className == 'page-link');
  expect(paginationLinks.length).toBe(8);
  expect(paginationLinks[1].children).toEqual(['20']);
  expect(paginationLinks[2].children).toEqual(['21']);
  expect(paginationLinks[3].children).toEqual(['22']);
  expect(paginationLinks[4].children).toEqual(['23']);
  expect(paginationLinks[5].children).toEqual(['24']);
  expect(paginationLinks[6].children).toEqual(['25']);
});

test('Paginator initializes with 10 pages correctly when only 3 should show', () => {
  var clickedPage = null;
  const component = renderer.create(
    <Paginator
      activePage={0}
      itemsCountPerPage={20}
      totalItemsCount={50}
      pageRangeDisplayed={10}
      onChange={page => (clickedPage = page)}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Paginator initializes with 5 pages correctly', () => {
  const component = renderer.create(
    <Paginator
      activePage={0}
      itemsCountPerPage={20}
      totalItemsCount={500}
      pageRangeDisplayed={5}
      onChange={() => true}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
