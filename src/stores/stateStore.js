import { extendObservable, action, set } from 'mobx';
import { reset } from 'ansi-colors';

const emptyState = {
  navIsOpen: false,
  beers: [],
  styles: [],
  breweries: [],
  beersPage: null,
  getBeersError: null,
};

class StateStore {
  constructor(state = {}) {
    extendObservable(this, emptyState, state);
  }

  /* Put actions and such here */
  @action
  reset() {
    set(this, emptyState);
  }
}

export default new StateStore();

export { StateStore };
