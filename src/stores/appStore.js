import { action, computed } from 'mobx';

const valueFromEvent = event => {
  if (event && event.target && typeof event.target.value !== 'undefined') {
    return event.target.value;
  }
  return event;
};

class AppStore {
  constructor(state) {
    this.state = state;
  }

  /* Put actions and such here */
  async loadBeers(page) {
    page = page ? page : 0;
    fetch(`http://localhost:8080/api/v1/beer?page=${page}&sort=name`)
      .then(res => res.json())
      .then(res => {
        this.setBeersPage({
          totalElements: res.totalElements,
          totalPages: res.totalPages,
          last: res.last,
          size: res.size,
          first: res.first,
          numberOfElements: res.numberOfElements,
          number: res.number,
          sort: res.sort,
        });
        return res.content;
      })
      .then(content => {
        this.setBeers(content);
      })
      .catch(err => {
        console.log('Err: ', err);
        this.setBeersError(err);
      });
  }

  async toggleNavOpen() {
    this.setNavOpen({ isOpen: !this.state.isOpen });
  }

  @action
  setBeers = event => {
    this.state.beers = valueFromEvent(event);
  };

  @action
  setBeersPage = event => {
    this.state.beersPage = valueFromEvent(event);
  };

  @action
  setBeersError = event => {
    this.state.getBeersError = valueFromEvent(event);
  };

  @action
  setNavOpen = event => {
    this.state.navIsOpen = valueFromEvent(event);
  };
}

export default AppStore;
