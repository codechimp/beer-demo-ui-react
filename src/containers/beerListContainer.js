import React from 'react';
import { inject } from 'mobx-react';

import BeerList from '../components/beerList';

// TODO: Need to refactor this container out...don't need it when using MobX
@inject('appStore', 'stateStore')
class BeerListContainer extends React.Component {
  componentDidMount() {
    const { appStore } = this.props;
    appStore.loadBeers();
    console.log('props: ', this.props);
  }

  handlePageChange(page) {
    console.log(`Changing to page: ${page}`);
    console.log('this: ', this);
    this.props.appStore.loadBeers(page - 1); // We subtract 1 because the backend indexes pages from 0, so we need to shift
  }

  render() {
    console.log('props: ', this.props);
    return <BeerList handlePageChange={this.handlePageChange.bind(this)} />;
  }
}

export default BeerListContainer;
