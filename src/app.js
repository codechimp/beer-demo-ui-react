import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';

import { appStore, stateStore } from './stores';


import Home from './components/home';
import BeersPage from "./components/beersPage";

const stores = { appStore, stateStore }
window.stateStore = stateStore;
configure({ enforceActions: 'observed'})

export default class App extends React.Component {
    render() {
        return (
            <Provider {...stores}>
                <Switch>
                    <Route exact path="/" component={ Home } />
                    <Route path="/beers" component={ BeersPage } />
                    <Route path="/admin" component={ BeersPage } />
                </Switch>
            </Provider>
        );
    }
}